# IntroToML3 ex3: Data visualisation

import matplotlib.pyplot as plt
import numpy as np


# 0과 5 사이의 수를 촘촘히 표현하는 방법
x = np.linspace(0, 5, 5) # np.linspace(a, b, c); a, b 사이를 c 등분

print(np.shape(x)) # x는 어떤 모양일까?
print(x) # x 안에는 어떤 값이 들어 있을까?


# 함수를 정의하기
def f(x):
    return x - 1
def g(x):
    return x*x - x + 1


plt.plot(x, f(x), label="f(x)")
plt.plot(x, g(x), label="g(x)")

plt.legend()

plt.show()

