import matplotlib.pyplot as plt
import numpy as np
import requests # HTTP request
import re # r(egular)e(xpression)

api_url = 'http://openapi.data.go.kr/openapi/service/rest/Covid19/getCovid19InfStateJson'
api_key = 'hl1wI%2BMOkaXdFTGbs23wrhSRqEL0UzwkOuNAGX0gy%2BdhklSsudbZj8Vt507VlIceMcji%2BIHm4HSqDdbtq1U5VA%3D%3D'
query = '&startCreateDt=20200101&endCreateDt=20201016'


url = api_url + '?serviceKey=' + api_key + query


# get 요청에 의해 발생한 response 얻기
res = requests.get(url)
raw_data = res.text

# 확진자 수 decideCnt
# 검사자 수 examCnt
# 사망자 수 deathCnt
# 데이터 생성 일자 createDt
decideCnt_text = re.findall(r'<decideCnt>(.*?)</decideCnt>', raw_data) # 확진자 수
examCnt_text = re.findall(r'<examCnt>(.*?)</examCnt', raw_data) # 검사 수
deathCnt_text = re.findall(r'<deathCnt>(.*?)</deathCnt>', raw_data) # 사망자 수
createDt_text = re.findall(r'<createDt>(2020-.*?)\s.*?</createDt>', raw_data) # 데이터 생성일


decideCnt = [] # x
examCnt = []
deathCnt = []
crateDt = createDt_text

# convert unit
for i in range(len(decideCnt_text)):
	decideCnt.append(int(decideCnt_text[i]))
	examCnt.append(int(examCnt_text[i]))
	deathCnt.append(int(deathCnt_text[i]))


def loss_func(x, y, a, b):
    h = a*x + b # model
    loss = (h - y)*(h - y)
    return np.sum(loss)

def grad_loss(x, y, a, b):
    #grad_x = 2*np.sum((a*x + b - y) * x)
    #grad_y = 2*np.sum(a*x + b - y)
    grad_x = 2*np.sum(a*x*x) + 2*np.sum(b*x) - 2*np.sum(y*x)
    grad_y = 2*np.sum(a*x) + 2*(len(x))*b - 2*np.sum(y)
    
    return grad_x, grad_y

def next_step(x, y, a, b):
    grad_x, grad_y = grad_loss(x, y, a, b)
    
    next_a = a - grad_x * 1e-10
    next_b = b - grad_y * 1e-10
    
    return next_a, next_b




#data
x = np.array(range(len(decideCnt)))
y = np.flip(decideCnt)

#normalise
#y = y/np.max(y)


#initial value
a = 60
b = 0

for i in range(10000):    
    a, b = next_step(x, y, a, b)
    print("Epoch: %d; a: %f, b: %f; loss: %f" % (i, a, b, loss_func(x, y, a, b)))



t = np.linspace(0, len(decideCnt), 1000)
plt.plot(x, y, 'o', label="decideCnt")
plt.plot(t, a*t + b, label="best-fit line: y = %.3fx + %.3f" % (a, b))
plt.legend()
plt.show()