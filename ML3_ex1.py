# IntroToML3 ex1: Data visualisation

import matplotlib.pyplot as plt



# 주어진 데이터
# {(x, y)} = {(1, 2), (3, 5), (6, 7), (10, 15), (12, 17)}


x = []
y = []
x = [1, 3, 6, 10, 12]
y = [2, 5, 7, 15, 17]


plt.plot(x, y)
plt.show()
