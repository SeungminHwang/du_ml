import matplotlib.pyplot as plt
import numpy as np


def loss_func(x, y, a, b):
    h = a*x + b # model
    loss = (h - y)*(h - y)
    return np.sum(loss)

def grad_loss(x, y, a, b):
    grad_x = 2*np.sum((a*x + b - y) * x)
    grad_y = 2*np.sum(a*x + b - y)
    
    return grad_x, grad_y

def next_step(x, y, a, b):
    grad_x, grad_y = grad_loss(x, y, a, b)
    
    next_a = a - grad_x * 0.01
    next_b = b - grad_y * 0.01
    
    return next_a, next_b




#data
x = np.array([1, 2, 3, 4, 5])
y = np.array([1, 3, 4, 3, 4])

#initial value
a = -5
b = -5

for i in range(1000):    
    a, b = next_step(x, y, a, b)
    print("Epoch: %d; a: %f, b: %f; loss: %f" % (i, a, b, loss_func(x, y, a, b)))



t = np.linspace(0, 6, 1000)
plt.plot(x, y, 'o')
plt.plot(t, a*t + b, label="y = 0.6x + 2.4")
plt.legend()
plt.show()