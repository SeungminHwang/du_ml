import requests # HTTP request
import re # r(egular)e(xpression)



# API 사용을 위한 유효한 URL 생성하기
# 'http://openapi.data.go.kr/openapi/service/rest/Covid19/getCovid19InfStateJson'
# 쿼리 스트링 시작(?) : ?serviceKey=
# 오픈 API 키 : 'hl1wI%2BMOkaXdFTGbs23wrhSRqEL0UzwkOuNAGX0gy%2BdhklSsudbZj8Vt507VlIceMcji%2BIHm4HSqDdbtq1U5VA%3D%3D'
# 요청하고 싶은 자료 : '&startCreateDt=20200101&endCreateDt=20201008'

api_url = 'http://openapi.data.go.kr/openapi/service/rest/Covid19/getCovid19InfStateJson'
api_key = 'hl1wI%2BMOkaXdFTGbs23wrhSRqEL0UzwkOuNAGX0gy%2BdhklSsudbZj8Vt507VlIceMcji%2BIHm4HSqDdbtq1U5VA%3D%3D'
query = '&startCreateDt=20200101&endCreateDt=20201016'


url = api_url + '?serviceKey=' + api_key + query


# get 요청에 의해 발생한 response 얻기
#res = requests.get(url)
res = requests.get('https://naver.com')
print(res)
#print(res.text)

raw_data = res.text

# 확진자 수 decideCnt
# 검사자 수 examCnt
# 사망자 수 deathCnt
# 데이터 생성 일자 createDt
decideCnt_text = re.findall(r'<decideCnt>(.*?)</decideCnt>', raw_data) # 확진자 수
examCnt_text = re.findall(r'<examCnt>(.*?)</examCnt', raw_data) # 검사 수
deathCnt_text = re.findall(r'<deathCnt>(.*?)</deathCnt>', raw_data) # 사망자 수
createDt_text = re.findall(r'<createDt>(2020-.*?)\s.*?</createDt>', raw_data) # 데이터 생성일


decideCnt = []
examCnt = []
deathCnt = []
crateDt = createDt_text

# convert unit
for i in range(len(decideCnt)):
	decideCnt.append(int(decideCnt_text[i]))
	examCnt.append(int(examCnt_text[i]))
	deathCnt.append(int(deathCnt_text[i]))


# graphics
fig, ax = plt.subplots()
ax.fmt_xdata = mdates.DateFormatter('%m-%d')
ax.xaxis.set_major_locator( mdates.MonthLocator() )

ax.plot_date(createDt, decideCnt, '-', label = "Positive")
ax.plot_date(createDt, examCnt, '-', label = "In test")
ax.plot_date(createDt, deathCnt, '-', label = "death Count")


plt.xticks(rotation = 20)


ax.legend()

plt.show()

