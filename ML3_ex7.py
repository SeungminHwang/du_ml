import numpy as np


def loss_func(x, y, a, b):
    h = a*x + b # model
    loss = (h - y)*(h - y)
    return np.sum(loss)

x = np.array([1, 2, 3, 4, 5])
y = np.array([1, 3, 4, 3, 4])

print("case1: ", loss_func(x, y, 0.5, 1.7))
print("case2: ", loss_func(x, y, 2, -2))
