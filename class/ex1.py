#{(x, y)} = {(1, 2), (3, 5), (6, 7), (10, 15), (12, 17)}

import matplotlib.pyplot as plt

x = [1, 3, 6, 10, 12]
y = [2, 5, 7, 15, 17]


plt.plot(x, y)


plt.show()