import numpy as np
import matplotlib.pyplot as plt

def loss_func(x, y, a, b):
    h = a*x + b
    sq_error = (h - y)*(h - y)
    return np.sum(sq_error)

def grad_loss(x, y, a, b):
    grad_x = 2*np.sum((a*x + b - y) * x)
    grad_y = 2*np.sum(a*x + b - y)
    
    return grad_x, grad_y

def next_step(x, y, a, b):
    grad_x, grad_y = grad_loss(x, y, a, b)
    
    next_a = a - grad_x * 0.001
    next_b = b - grad_y * 0.001
    
    return next_a, next_b

x = np.array([1, 2, 3, 4, 5, 6])
y = np.array([1, 3, 2, 1, 4, 3])
'''
a1 = 0.5
b1 = 1
print("model1: ", loss_func(x, y, a1, b1))

a2 = 2
b2 = -2
print("model2: ", loss_func(x, y, a2, b2))
'''
a = 5
b = -5

for i in range(10000):
    a, b = next_step(x, y, a, b)
    print("Epoch: %d; a: %f, b: %f; loss: %f" %(i, a, b, loss_func(x, y, a, b)))


t = np.linspace(0, 6, 1000)
plt.plot(x, y, 'o')
plt.plot(t, a*t + b)
plt.show()
