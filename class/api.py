import requests
import re
import matplotlib.pyplot as plt

svc_url = 'http://openapi.data.go.kr/openapi/service/rest/Covid19/getCovid19InfStateJson'
key = 'hl1wI%2BMOkaXdFTGbs23wrhSRqEL0UzwkOuNAGX0gy%2BdhklSsudbZj8Vt507VlIceMcji%2BIHm4HSqDdbtq1U5VA%3D%3D'
query_string = '&startCreateDt=20200101&endCreateDt=20201016'

url = svc_url + '?' + 'ServiceKey=' + key + query_string

res = requests.get(url)
raw_data = res.text

decideCnt_text = re.findall(r'<decideCnt>(.*?)</decideCnt>', raw_data)
#print(decideCnt_text)

decideCnt = []
for i in range(len(decideCnt_text)):
    decideCnt.append(int(decideCnt_text[i]))
    
print(decideCnt)

x = range(len(decideCnt))
y = decideCnt[::-1]
plt.plot(x, y)
plt.show()