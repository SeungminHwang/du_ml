import matplotlib.pyplot as plt
import numpy as np

'''
f(x) = x - 1 의 그래프는 어떤 모습일까요?
g(x) = x*x - x + 1 의 그래프는 어떤 모습일까요?
'''

def f(x):
    return x - 1
def g(x):
    return x*x - x + 1
    

x = np.linspace(0, 5, 1000)
print(x) # [0.   1.25 2.5  3.75 5.  ]

y1 = f(x)
y2 = g(x)

plt.plot(x, y1)
plt.plot(x, y2)

plt.show()
